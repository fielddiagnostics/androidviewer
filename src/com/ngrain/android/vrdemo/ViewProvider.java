package com.ngrain.android.vrdemo;

import android.view.View;

/**
 * Interface that allows IoC registration module to find native Android views.
 */
public interface ViewProvider
{
    View getOverlayElementById(int resId);
}
