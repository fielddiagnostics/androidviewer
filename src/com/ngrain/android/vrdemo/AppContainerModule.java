package com.ngrain.android.vrdemo;

import org.picocontainer.MutablePicoContainer;

import android.content.Context;

import com.ngrain.animations.ioc.AnimationsContainerModule;
import com.ngrain.parts.ioc.PartsContainerModule;
import com.ngrain.tasks.ioc.TasksContainerModule;

public class AppContainerModule
{
    /**
     * Component registration once renderer is initialized and Android views are available.
     */
    public static void RegisterOnResume(
        MutablePicoContainer container,
        ViewProvider viewProvider,
        Context context)
    {
        PartsContainerModule.Register(container);
        PartsContainerModule.RegisterViews(container, viewProvider, context);
        AnimationsContainerModule.Register(container);
        TasksContainerModule.RegisterDiscovery(container);
        TasksContainerModule.Register(container);
        TasksContainerModule.RegisterViews(container, viewProvider, context);
    }
}
