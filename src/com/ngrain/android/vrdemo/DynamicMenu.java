package com.ngrain.android.vrdemo;

import android.os.Handler;
import android.os.Looper;

/**
 * Dynamic menu class that wraps around the native Android options menu
 * and allows run-time insertion of discovered tasks.
 */
public class DynamicMenu implements com.ngrain.android.vrdemo.Menu
{
    private final android.view.Menu mWrappedMenu;
    private final Handler mHandler = new Handler(Looper.getMainLooper());

    public DynamicMenu(android.view.Menu wrappedMenu)
    {
        mWrappedMenu = wrappedMenu;
    }

    public void insertMenuItem(
        int menuItemId,
        String title,
        int iconId,
        int parent)
    {
        Runnable runnable = new MenuInsertRunnable(menuItemId, title);
        mHandler.post(runnable);
    }
 
    private class MenuInsertRunnable implements Runnable
    {
        private int mMenuItemId;
        private String mTitle;

        public MenuInsertRunnable(int menuId, String title)
        {
            mMenuItemId = menuId;
            mTitle = title;
        }

        @Override
        public void run()
        {
            mWrappedMenu.add(0, mMenuItemId, 0, mTitle);
        }
    }
}
