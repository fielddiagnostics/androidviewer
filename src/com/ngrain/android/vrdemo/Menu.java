package com.ngrain.android.vrdemo;

/**
 * Interface for a menu that allows run-time insertion of items.
 */
public interface Menu
{
    public void insertMenuItem(
        int menuItemId,
        String title,
        int iconId,
        int parent);
}
