package com.ngrain.android.vrdemo;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;

import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.SoundEffectConstants;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ngrain.camera.contracts.CameraController;
import com.ngrain.parts.PartDetailsController;
import com.ngrain.parts.contracts.PartPicker;
import com.ngrain.scene.contracts.SceneController;
import com.ngrain.tasks.TaskSelectionController;
import com.ngrain.tasks.contracts.TaskDiscovery;
import com.ngrain.tasks.contracts.TaskPlayer;

import org.picocontainer.DefaultPicoContainer;
import org.picocontainer.MutablePicoContainer;
import org.picocontainer.behaviors.Caching;

/**
 * Main Android activity. Responsible for all initializations such as the C++
 * voxel-based renderer. Handles user interaction at the highest-level. 
 */
public class MainActivity extends Activity
    implements SessionListener, ViewProvider
{
    //IoC container for registering dependencies and resolving components
    private MutablePicoContainer mOnResumeContainer;
    
    //Indicates whether renderer has initialized
    private boolean mRendererInitialized = false;

    //Controllers that the main activity directly interacts with
    private TaskSelectionController mTaskSelectionController;
    private PartDetailsController mPartDetailsController;
    private PartPicker mPartPicker;
    private TaskPlayer mProcedure;

    //Dynamic options menu that can be populated by list of tasks
    private com.ngrain.android.vrdemo.Menu mMenu;
    
    //Tracks whether options menu is opened.
    private boolean mIsMenuOpened = false;
    
    //Main text view controlled by the activity for displaying status
    private TextView mInfoTextView;
    
    //The GL Surface for rendering
    public MainGLSurfaceView mGLView;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        
        //Load NGRAIN C++ library
        System.loadLibrary("NGrain");

        //This activity is expected to be launched from FileListActivity, providing
        //a selected model file path.
        String modelPath = getIntent().getStringExtra(FileListActivity.MODEL_PATH_DATA);
        
        //If model path is not found, the activity has been launched directly
        //by another app without providing a path.
        if( modelPath == null)
        {
            //Launch FileListActivity activity to request user to select a model
            Intent intent = new Intent(this, FileListActivity.class);
            startActivity(intent);
            finish();
            return;
        }
        
        Log.i("MainActivity", "Model path: " + modelPath);
        
        //Creates the GL Surface for rendering. The surface view also
        //contains renderer initialization code.
        mGLView = new MainGLSurfaceView(this, modelPath);
        setContentView(mGLView);
        mGLView.sessionListener = this;
               
        RelativeLayout.LayoutParams params =
          new RelativeLayout.LayoutParams(
            LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);

        View infoLayout = getLayoutInflater().inflate(
            R.layout.part_info_overlay_layout, null);
        addContentView(infoLayout, params);
        
        mInfoTextView = (TextView) getOverlayElementById(R.id.infoTextView);
        mInfoTextView.setText("Model is loading ...");
    }
    
    //The following callback gets called when the renderer 
    //has completed loading the model.
    public void onSessionInitialized()
    {
        mRendererInitialized = true;
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.main, menu);
        mMenu = new DynamicMenu(menu);
        
        //Disable all menu items until GL surface is initialized
        for(int i = 0; i < menu.size(); i++)
        {
            menu.getItem(i).setEnabled(false);
        }
        
        return true;
    }
    
    @Override
    public boolean onPrepareOptionsMenu (Menu menu)
    {
        super.onPrepareOptionsMenu(menu);
        
        //onPrepareOptionsMenu() is called each time the user opens the 
        //options menu. Enable menu items, and add task list only after 
        //all initializations has been performed.
        if( (mOnResumeContainer != null) && (mTaskSelectionController == null))
        {
            for(int i = 0; i < menu.size(); i++)
            {
                menu.getItem(i).setEnabled(true);
            }
            
            mTaskSelectionController = new TaskSelectionController(
                mOnResumeContainer.getComponent(TaskDiscovery.class), 
                mMenu);
        }
        
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        //Check if a task was selected
        int taskMenuItemId = item.getItemId();
        String taskId = mTaskSelectionController.getSelectedTaskId(taskMenuItemId);
        if (taskId != null)
        {
            resetControllers();
            startTask(taskId);
            return true;
        }
        
        //Other menu items:
        int itemId = item.getItemId();
        switch (itemId)
        {
            case R.id.reset_menu_item:
                resetControllers();
                mGLView.ResetCameraAndScene();
                return true;
        }
        
        return super.onOptionsItemSelected(item);
    }
    
    @Override
    public boolean onMenuOpened(int featureId, Menu menu)
    {
        if (featureId == Window.FEATURE_ACTION_BAR)
        {
            mIsMenuOpened = true;
        }
        
        return super.onMenuOpened(featureId, menu);
    }
    
    @Override
    public void onPanelClosed(int featureId, Menu menu) 
    {
        if (featureId == Window.FEATURE_ACTION_BAR)
        {
            mIsMenuOpened = false;
        }
        
        super.onPanelClosed(featureId, menu);
    }
    
    @Override
    protected void onResume()
    {
        super.onResume();
        mGLView.onResume();
        mGLView.requestFocus();

        mGLView.post(mResumeRunnable);
    }
    
    private Runnable mResumeRunnable = new Runnable()
    {
        public void run() 
        {
            //If renderer is not yet initialized, don't
            //continue with IoC initialization. Post delay 
            //the action by a certain time and check again.
            if( mRendererInitialized == false )
            {
                mGLView.postDelayed(mResumeRunnable, 100);
            }
            else
            {
                setupOnResume();
            }
        }
    };
    
    private void setupOnResume()
    {
        //Register mMessageReceiver to receive messages only when this activity is the current activity.
        //Register for myo armband events sent from DRDC MoverioApp.
        String myoEventArrived = getResources().getText(R.string.myo_event_arrived).toString();
        registerReceiver(mMessageReceiver, new IntentFilter(myoEventArrived));
        
        //The following are one-time setup, early return if the setup has already been done
        if (mOnResumeContainer != null)
        {
            return;
        }
     
        // Register listeners to GL Surface View to handle user interactions.
        mGLView.setOnKeyListener(mGLSurfaceKeyPressListener);
        mGLView.setOnGestureListener(mGLSurfaceGestureEventListener);
        showInfoTextView(true);
        
        // Most Java modules are instantiated using Inversion of Control container.
        // Caching means that when an object is requested, either explicitly
        // or via dependency, you get the same instance. Otherwise each
        // request returns a new instance.
        mOnResumeContainer = new DefaultPicoContainer(new Caching());

        // Register modules
        mOnResumeContainer.addComponent(mGLView); //Concrete component for both Camera and SceneController  
        AppContainerModule.RegisterOnResume(mOnResumeContainer, this, this);

        // Resolve components
        mPartPicker = mOnResumeContainer.getComponent(PartPicker.class);
        mPartDetailsController = mOnResumeContainer.getComponent(PartDetailsController.class);
        mProcedure = mOnResumeContainer.getComponent(TaskPlayer.class);
    }
    
    @Override
    protected void onPause()
    {
        super.onPause();
        mGLView.onPause();
        
        // Deregister from BroadcastManager to prevent getting messages at unwanted times
        unregisterReceiver(mMessageReceiver);
    }
    
    @Override
    protected void onStart()
    {
        super.onStart();
    }
    
    @Override
    protected void onStop()
    {
        resetControllers();
        super.onStop();
    }
    
    @Override
    protected void onDestroy()
    {
        super.onDestroy();
    }

    //Assumes we start from controller reset state
    private void startTask(String taskId)
    {
        showInfoTextView(false);
        mProcedure.startTask(taskId);
    }
    
    //Resets all controllers, deselects any parts, stops any task playback
    private void resetControllers()
    {
        showInfoTextView(true);
        mPartPicker.deselectParts();
        mPartDetailsController.reset();
        mProcedure.stopTask();
    }
    
    public View getOverlayElementById(int resId)
    {
        return this.findViewById(resId);
    }
    
    private void showInfoTextView(boolean show)
    {
        if( show )
        {
            mInfoTextView.setVisibility(View.VISIBLE);
            mInfoTextView.setText(R.string.default_primary_information);
        }
        else
        {
            mInfoTextView.setVisibility(View.GONE);
            mInfoTextView.setText("");
        }
    }
    
    //Message receiver for handling myo events broadcasted from DRDC Moverio App
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() 
    {
        @Override
        public void onReceive(Context context, Intent intent) 
        {
            // Extract data included in the Intent
            String myoGesture = intent.getStringExtra("gesture");
            Log.i("MainActivity", "Got message: " + myoGesture);
            
            if( mIsMenuOpened )
            {
                //Move up the options menu
                if( myoGesture.equals("wave in") )
                {
                    simulateKeyPress(KeyEvent.KEYCODE_DPAD_UP);
                }
                //Move down the options menu
                else if ( myoGesture.equals("wave out") )
                {
                    simulateKeyPress(KeyEvent.KEYCODE_DPAD_DOWN);  
                }
                //Exit the options menu
                else if ( myoGesture.equals("fingers spread") )
                {
                    closeOptionsMenu();
                }
                //Selects the option
                else if ( myoGesture.equals("fist") )
                {
                    simulateKeyPress(KeyEvent.KEYCODE_ENTER); 
                }
            }
            else
            {
                //Open the options menu
                if ( myoGesture.equals("fingers spread") )
                {
                    openOptionsMenu();
                }
                //If we are in task playing state
                else if (mProcedure.isPlayingTask())  
                {
                    if ( myoGesture.equals("wave out") )
                    {
                        mGLView.playSoundEffect(SoundEffectConstants.CLICK);
                        
                        boolean hasNextStep = mProcedure.nextTaskStep();
                        if( !hasNextStep )
                        {
                            mInfoTextView.setVisibility(View.VISIBLE);
                            mInfoTextView.setText("Task completed.");
                        }
                    }
                }
                else if ( myoGesture.equals("wave in") )
                {
                    simulateKeyPress(KeyEvent.KEYCODE_DPAD_LEFT);
                }
                else if ( myoGesture.equals("wave out") )
                {
                    simulateKeyPress(KeyEvent.KEYCODE_DPAD_RIGHT);
                }
            }
        }
    };
    
    private void simulateKeyPress(final int keyCode)
    {
      new Thread(new Runnable() 
      {         
          @Override
          public void run() 
          {
              Instrumentation inst = new Instrumentation();
              inst.sendKeyDownUpSync(keyCode);
          }   
      }).start();
    }
    
    private OnKeyListener mGLSurfaceKeyPressListener = new OnKeyListener()
    {
        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event)
        {
            float amount = 22.5f;
            if(keyCode == KeyEvent.KEYCODE_DPAD_RIGHT)
            {
                mGLView.OrbitCamera(-amount, 0);
                return true;
            }
            else if(keyCode == KeyEvent.KEYCODE_DPAD_LEFT)
            {
                mGLView.OrbitCamera(amount, 0);
                return true;
            }
            else if(keyCode == KeyEvent.KEYCODE_DPAD_UP)
            {
                mGLView.OrbitCamera(0, amount);
                return true;
            }
            else if(keyCode == KeyEvent.KEYCODE_DPAD_DOWN)
            {
                mGLView.OrbitCamera(0, -amount);
                return true;
            }

            return false;
        }
    };
    
    private MainGLSurfaceView.EventListener mGLSurfaceGestureEventListener = new MainGLSurfaceView.EventListener()
    {
        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) 
        {
            //Plays click sound when tap detected. Not necessary if 
            //event handling code is in onClick() because performClick()
            //of the view (which triggers onClick()) will play the sound for you.
            mGLView.playSoundEffect(SoundEffectConstants.CLICK);
            
            //If we are in task playing state
            if (mProcedure.isPlayingTask())  
            {
                boolean hasNextStep = mProcedure.nextTaskStep();
                if( !hasNextStep )
                {
                    mInfoTextView.setVisibility(View.VISIBLE);
                    mInfoTextView.setText("Task completed.");
                }
                
                return true;
            }
            else  //Normal part selection state
            {
                float x = mGLView.scaleXToCoreWidth(e.getX());
                float y = mGLView.scaleYToCoreHeight(e.getY());
                int partId = mGLView.PickPartFromScreen(x, y);
                mPartPicker.pickPart(partId);
                mPartDetailsController.updateSelection(partId);
                
                if( partId != -1 )
                {
                    showInfoTextView(false);
                }
                else
                {
                    showInfoTextView(true);
                }
    
                return true;
            }
        }
        
        @Override
        public void onLongPress(MotionEvent e) 
        {
            openOptionsMenu();
        }
        
        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
        {
            if (e2.getPointerCount() > 1)   
            {
                float x = -2*mGLView.scaleXToCoreWidth(distanceX);
                float y = -2*mGLView.scaleYToCoreHeight(distanceY);
                mGLView.PanCamera(x, y);
            }
            else
            {
                float x = -mGLView.scaleXToCoreWidth(distanceX);
                float y = -mGLView.scaleYToCoreHeight(distanceY);
                mGLView.OrbitCamera(x, y);
                //Log.i("MainActivity", String.format("orbit : %f %f", x, y));
            }
            return true;
        }
        
        @Override
        public boolean onScaleBegin(ScaleGestureDetector detector)
        {
            return true;
        }
        
        @Override
        public boolean onScale(ScaleGestureDetector detector)
        {
            float scaleFactor = detector.getScaleFactor();
            mGLView.ZoomCamera((float) Math.pow(scaleFactor, 4));
            return true;
        }
        
        @Override
        public void onScaleEnd(ScaleGestureDetector scaleGestureDetector)
        {
            return;
        }
    }; 
}
