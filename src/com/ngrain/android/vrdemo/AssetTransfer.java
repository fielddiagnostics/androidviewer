package com.ngrain.android.vrdemo;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

public class AssetTransfer
{
    //Copies assets to internal storage and returns the storage's absolute path 
    public static String copyModelAssetsToInternalStorage(Context context)
    {
        AssetManager assetManager = context.getAssets();
        String appRootPath = context.getFilesDir().getPath().toString();
        String storageFolderPath = appRootPath + "/../";
        List<String> modelFiles = new ArrayList<String>();

        //Clean old models from destination folder first
        File storageFolder = new File(storageFolderPath);
        {
            String[] children = storageFolder.list(mModelFileFilter);
            for (int i = 0; i < children.length; i++)
            {
               new File(storageFolder, children[i]).delete();
            }
        }
        
        //Grab models from asset
        try
        {
            String[] allFiles = assetManager.list("");
            for(String filename : allFiles)
            {
                if( filename.endsWith(".ako") || filename.endsWith(".AKO") )
                {
                    modelFiles.add(filename);
                }
            }
        }
        catch (IOException e)
        {
            Log.i("AssetTransfer", "Failed to get asset model list", e);
        }

        // loop through each file and copy to the application root
        for (String filename : modelFiles)
        {
            String filePath = storageFolderPath + filename;
            
            try
            {
                InputStream in = null;
                in = assetManager.open(filename);

                BufferedInputStream bin = null;
                BufferedOutputStream bout = null;
                try
                {
                    bin = new BufferedInputStream(in);
                    try
                    {
                        bout = new BufferedOutputStream(
                                new FileOutputStream(filePath));
                        
                        copyFile(bin, bout);
                    }
                    finally
                    {
                        if (bout != null)
                            bout.close();
                    }
                }
                finally
                {
                    if (bin != null)
                        bin.close();
                }
                in.close();

                Log.i("AssetTransfer", "Asset model copied to: " + filePath);
            }
            catch (IOException e)
            {
                Log.i("AssetTransfer", "Failed to copy asset model to: " + filePath, e);
            }
        }
        
        return storageFolderPath;
    }

    private static void copyFile(InputStream in, OutputStream out) throws IOException
    {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1)
        {
            out.write(buffer, 0, read);
        }
    }
    
    private static FilenameFilter mModelFileFilter = new FilenameFilter() 
    {
        public boolean accept(File dir, String name) 
        {
            if(name.endsWith(".ako") || name.endsWith(".AKO")) 
            {
                return true;
            }
            
            return false;
        }
    };
}
