package com.ngrain.android.vrdemo;

/**
 * Interface for GLSurfaceView to notify a listener that
 * renderer initialization is complete.
 */
public interface SessionListener
{
    void onSessionInitialized();
}
