package com.ngrain.android.vrdemo;


import android.content.Context;
import android.opengl.GLSurfaceView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.View.OnAttachStateChangeListener;
import android.view.GestureDetector;

import com.ngrain.camera.contracts.CameraController;
import com.ngrain.camera.contracts.CameraProperties;
import com.ngrain.scene.contracts.PartSceneInfo;
import com.ngrain.scene.contracts.SceneController;

/**
 * Main rendering surface. Encapsulates the renderer, responsible for its 
 * initialization and makes native calls into the renderer.
 */
class MainGLSurfaceView extends GLSurfaceView
    implements 
        OnAttachStateChangeListener, 
        SessionListener, 
        CameraController,
        SceneController
{
    /**
     * Listener to be notified when renderer completed loading the model file.
     */ 
    public SessionListener sessionListener;

    public MainGLSurfaceView(Context context, String modelFilePath) 
    {
        super(context);
        
        setEGLContextClientVersion(2);
        
        mRenderer = new MainRenderer(modelFilePath);
        mRenderer.sessionListener = this;
        setRenderer(mRenderer);
        setRenderMode(RENDERMODE_CONTINUOUSLY);
        mCoreViewportWidth = mRenderer.getCoreViewportWidth();
        mCoreViewportHeight = mRenderer.getCoreViewportHeight();
        
        // must set the view to be focusable
        setFocusable(true);
        setFocusableInTouchMode(true);
    }

    /**
     * Called by renderer that initialization is complete.
     */
    public void onSessionInitialized() 
    {
        //The mRenderer is run by a rendering thread. Thus,
        //our onSessionInitialized event is called from the rendering
        //thread. The message posting ensures our onSessionListener()
        //callback will be executed on UI thread.
        this.post( new Runnable() 
        {
            public void run() 
            {
                sessionListener.onSessionInitialized();
            }
        });
    }

    @Override
    public void onViewAttachedToWindow(View v) {
        requestFocus();
    }

    @Override
    public void onViewDetachedFromWindow(View v) {
    }
    
    @Override
    public boolean performClick() {
        return super.performClick();
    }

    
    public boolean onTouchEvent(final MotionEvent event)
    {
        //Pass a MotionEvent into the gesture detector
        if( mTouchDetector == null || mScaleDetector == null )
        {
            return false;
        }
        
        mTouchDetector.onTouchEvent(event);
        mScaleDetector.onTouchEvent(event);
        return true;
    }
    
    /**
     * Set listener for gesture events
     */
    public void setOnGestureListener(EventListener listener)
    {
        initGestureDetectors(getContext(), listener);
    }

    /**
     * Callback interface for GL Surface View to notify any listener of
     * user gesture input events
     */
    public interface EventListener 
    {
        public boolean onSingleTapConfirmed(MotionEvent e);
        public void onLongPress(MotionEvent e);
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY);
        public boolean onScaleBegin(ScaleGestureDetector detector);
        public boolean onScale(ScaleGestureDetector detector);
        public void onScaleEnd(ScaleGestureDetector scaleGestureDetector);
    }
        
    //Create gesture detector that triggers onClickListener.
    private void initGestureDetectors(Context context, final EventListener listener) 
    {
        mTouchDetector = new GestureDetector(
            context, 
            new GestureDetector.SimpleOnGestureListener()
            {
                @Override
                public boolean onSingleTapConfirmed(MotionEvent e) 
                {
                    //Ensures any existing onClickListener is notified and performs other android system relevant calls.
                    performClick();
                    
                    //Notify external listener
                    listener.onSingleTapConfirmed(e);
                    
                    return true;
                }
                
                @Override
                public void onLongPress(MotionEvent e) 
                {
                    //Ensures any existing onLongClickListener is notified
                    performLongClick();
                    
                    //Notify external listener
                    listener.onLongPress(e);
                }
                
                @Override
                public boolean onScroll(
                    MotionEvent e1, 
                    MotionEvent e2, 
                    float distanceX, 
                    float distanceY)
                {
                    //Notify external listener
                    listener.onScroll(e1, e2, distanceX, distanceY);
                    return true;
                }
            });
        
        mScaleDetector = new ScaleGestureDetector(
            context, 
            new ScaleGestureDetector.SimpleOnScaleGestureListener() 
            {
                @Override
                public boolean onScaleBegin(ScaleGestureDetector detector) 
                {
                    listener.onScaleBegin(detector);
                    return true;
                }
    
                @Override
                public boolean onScale(ScaleGestureDetector detector) 
                {
                    listener.onScale(detector);
                    return true;
                }
    
                @Override
                public void onScaleEnd(ScaleGestureDetector detector) 
                {
                    listener.onScaleEnd(detector);
                }
            });
    }

    public void ResetCameraAndScene()
    {
        nativeResetCameraAndScene();
    }

    public void OrbitCamera(float amountX, float amountY)
    {
        nativeOrbitGesture(amountX, amountY);
    }

    public void YawCamera(float amount)
    {
        nativeOrbitGesture(amount, 0);
    }

    public void PanCamera(float amountX, float amountY)
    {
        nativePanGesture(amountX, amountY);
    }

    public void ZoomCamera(float amount)
    {
        nativeScaleGesture(amount);
    }
    
    public void setCameraProperties(CameraProperties properties)
    {
        nativeSetCameraProperties(properties);
    }
    
    public void setCameraTransform(float[] transformData)
    {
        nativeSetCameraTransform(transformData);
    }
    
    public void setSceneState(PartSceneInfo[] partsSceneInfo)
    {
        nativeSetSceneState(partsSceneInfo);
    }
    
    public PartSceneInfo[] getSceneState()
    {
        return nativeGetSceneState();
    }
    
    public void setContextMode(boolean contextMode)
    {
        nativeSetContextMode(contextMode);
    }
    
    public boolean getContextMode()
    {
        return nativeGetContextMode();
    }
    
    public int[] getPartIds()
    {
        return nativeGetPartIds();
    }
    
    public void clearSelections()
    {
        nativeClearSelections();
    }
    
    public void setSelection(int partId, boolean select)
    {
        nativeSetSelection(partId, select);
    }
    
    public boolean getSelection(int partId)
    {
        return nativeGetSelection(partId);
    }
    
    public void setTransforms(int[] partIds, float[] transformsData)
    {
        nativeSetTransforms(partIds, transformsData);
    }
    
    public float[] getTransforms(int[] partIds)
    {
        return nativeGetTransforms(partIds);
    }
    
    public int PickPartFromScreen(float x, float y)
    {
        int partId = nativePickPartFromScreen(Math.round(x), Math.round(y));
        
        if( partId < 0 ) return SceneController.NoPartPicked;
        
        return partId;
    }

    /**
     * Scales to core viewport coordinates given GL surface view location.
     */ 
    public float scaleXToCoreWidth(float x)
    {
        return x *  mCoreViewportWidth / this.getWidth();
    }
    
    /**
     * Scales to core viewport coordinates given GL surface view location.
     */ 
    public float scaleYToCoreHeight(float y)
    {
        return y * mCoreViewportHeight / this.getHeight();
    }

    private MainRenderer mRenderer;

    private float mCoreViewportWidth;
    private float mCoreViewportHeight;
    
    private GestureDetector mTouchDetector = null;
    private ScaleGestureDetector mScaleDetector = null;

    private static native void nativeOrbitGesture(float x, float y);
    private static native void nativePanGesture(float x, float y);
    private static native void nativeScaleGesture(float factor);
    private static native void nativeResetCameraAndScene();
    private static native void nativeSetCameraProperties(CameraProperties properties);
    private static native void nativeSetCameraTransform(float[] transformData);
    private static native void nativeSetSceneState(PartSceneInfo[] partsSceneInfo);
    private static native PartSceneInfo[] nativeGetSceneState();
    private static native void nativeSetContextMode(boolean contextMode);
    private static native boolean nativeGetContextMode();
    private static native int[] nativeGetPartIds();
    private static native void nativeClearSelections();
    private static native void nativeSetSelection(int partId, boolean select);
    private static native boolean nativeGetSelection(int partId);
    private static native void nativeSetTransforms(int [] partId, float[] transformsData);
    private static native float[] nativeGetTransforms(int [] partId);
    private static native int nativePickPartFromScreen(int x, int y);
}
