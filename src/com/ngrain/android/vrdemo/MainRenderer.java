package com.ngrain.android.vrdemo;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.opengl.GLSurfaceView;

class MainRenderer implements GLSurfaceView.Renderer
{
    public SessionListener sessionListener;
    
    private String mModelFilePath;
    private boolean mFileInitialized;

    public MainRenderer(String modelFilePath) 
    {
        super();

        mModelFilePath = modelFilePath;
        mFileInitialized = false;
    }
    
    //Note that the following calls are all executed by a renderer thread
    //different from the main ui thread.
    public void onSurfaceCreated(GL10 gl, EGLConfig config) 
    {   
        //Initialize C++ OpenGL shaders
        nativeInitGL();

        // JSB: With the nature of Android (or at least on Glass) the surface
        //  is recreated every time the screen awakens from sleep or lock. We
        //  definitely do not want to re-init the entire scene and camera setup
        //  every time because isn't very user friendly to do so.
        if (!mFileInitialized)
        {
            //Initialize Core with specified model file path
            nativeInitCore(mModelFilePath);
            mFileInitialized = true;
            if(sessionListener != null) 
            {
                sessionListener.onSessionInitialized();
            }
        }
        else
        {
            nativeRender();
        }
    }
    
    public void onSurfaceChanged(GL10 gl, int w, int h) 
    {
        //ML: Currently nativeResize() doesn't actually do anything. 
        //Our C++ rendering code does not support change in viewport size.
        //nativeResize(w, h);
    }
    
    public void onDrawFrame(GL10 gl) {
        nativeRender();
    }
    
    public int getCoreViewportWidth() {
        return nativeGetCoreViewportWidth();
    }
    
    public int getCoreViewportHeight() {
        return nativeGetCoreViewportHeight();
    }
    
    private static native void nativeInitGL();
    private static native void nativeInitCore(String modelFilePath);
    private static native void nativeResize(int w, int h);
    private static native void nativeRender();
    private static native void nativeDone();
    private static native int  nativeGetCoreViewportWidth();
    private static native int  nativeGetCoreViewportHeight();

    private static native double getCurrentFps();
    private static native double getMaxFps();
    private static native double getMinFps();
}