package com.ngrain.android.vrdemo;

import java.io.File;
import java.io.FilenameFilter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

/**
 * FileListActivity searches for available models and provides UI to prompt user to select one.
 */
public class FileListActivity extends Activity {

    static final String MODEL_PATH_DATA = "model_path_data";  // The name of returned data
    static final String NO_MODELS_FOUND = "No models found.";
    private File mNgrainFolder;
    private File mAssetsFolder;
    private ArrayAdapter<String> mDemoModelsArrayAdapter;
    private ArrayAdapter<String> mUserModelsArrayAdapter;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file_list);
        
        setFinishOnTouchOutside(false); 
        
        //Copy demo assets to folder that is accessible
        String assetsFolderPath = AssetTransfer.copyModelAssetsToInternalStorage(getApplicationContext());
        
        //List demo models in assets folder 
        mAssetsFolder = new File(assetsFolderPath);
        String demo_files[] = mAssetsFolder.list(mModelFileFilter);
        mDemoModelsArrayAdapter = new ArrayAdapter<String>(this, R.layout.model_path_list_item, demo_files);
        
        // Find and set up the ListView for demo models
        ListView demoModelsListView = (ListView) findViewById(R.id.list_demo_models);
        demoModelsListView.setAdapter(mDemoModelsArrayAdapter);
        demoModelsListView.setOnItemClickListener(mClickListener);
        
        //List user models in NGRAIN folder
        File sdcard = Environment.getExternalStorageDirectory();
        mNgrainFolder = new File(sdcard, "NGRAIN_VR");
        String user_files[] = mNgrainFolder.list(mModelFileFilter);
        if( user_files == null || user_files.length == 0)
        {
            String[] noFiles = {NO_MODELS_FOUND}; 
            mUserModelsArrayAdapter = new ArrayAdapter<String>(this, R.layout.model_path_list_item, noFiles);
        }
        else
        {
            mUserModelsArrayAdapter = new ArrayAdapter<String>(this, R.layout.model_path_list_item, user_files);
        }
        
        // Find and set up the ListView for user models
        ListView modelPathsListView = (ListView) findViewById(R.id.list_user_models);
        modelPathsListView.setAdapter(mUserModelsArrayAdapter);
        modelPathsListView.setOnItemClickListener(mClickListener);
    }
    
    private FilenameFilter mModelFileFilter = new FilenameFilter() 
    {
        public boolean accept(File dir, String name) 
        {
            if(name.endsWith(".ako") || name.endsWith(".AKO")) 
            {
                return true;
            }
            
            return false;
        }
    };
    
    private final AdapterView.OnItemClickListener mClickListener = new AdapterView.OnItemClickListener()
    {
        public void onItemClick(AdapterView<?> parent, View v, int position, long id) 
        {
            String filename = ((TextView) v).getText().toString();
            
            //Do nothing if NO_MODELS_FOUND
            if( filename.endsWith(NO_MODELS_FOUND))
            { 
                return;
            }
            
            File file = null;
            if( parent.getAdapter() == mUserModelsArrayAdapter)
            {
                file  = new File(mNgrainFolder, filename);
            }
            else if(parent.getAdapter() == mDemoModelsArrayAdapter )
            {
                file  = new File(mAssetsFolder, filename);
            }
            
            Log.i("FileListActivity", "Path selected: " + file.getAbsolutePath());
            
            //Launch MainActivity once a model is selected
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            intent.putExtra(MODEL_PATH_DATA, file.getAbsolutePath());
            startActivity(intent);
        }
    };
}
