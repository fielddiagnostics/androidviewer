package com.ngrain.scene.contracts;

/**
 * Contains all rendering parameter relating to a part.
 * The object is passed to @see SceneController to set/get
 * how a part is rendered.  
 */
public class PartSceneInfo 
{
    /**
     * Constructor
     * @param partId        Part ID of the part to be set
     * @param visible       Visibility of the part
     * @param selected      Highlighting of the part
     * @param transformData 4x4 transformation matrix of the part, 
     *                      stored in row order in a 16-elements float 
     *                      array               
     */
    public PartSceneInfo(
        int partId,
        boolean visible,
        boolean selected,
        float[] transformData )
    {
        mPartId = partId;
        mVisible = visible;
        mSelected = selected;
        mTransformData = transformData;
    }
    
    /**
     * Gets part ID.
     */
    public int getPartId()
    {
        return mPartId;
    }
    
    /**
     * Gets visibility state of the part.
     */
    public boolean getVisible()
    {
        return mVisible;
    }
    
    /**
     * Gets selection state of the part.
     */
    public boolean getSelected()
    {
        return mSelected;
    }
    
    /**
     * Gets transformation data.
     * @ return The transformation matrix is flattened into a 16-elements float array
     *          stored in row order.
     */
    public float[] getTransformData()
    {
        return mTransformData;
    }
    
    private final int mPartId;
    private final boolean mVisible;
    private final boolean mSelected;
    private final float[] mTransformData;
}
