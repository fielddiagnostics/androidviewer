package com.ngrain.scene.contracts;


/**
 * Allows one to change how a part is positioned and
 * rendered in a 3D scene.
 */
public interface SceneController 
{
    /**
     * Sets the rendering parameter for multiple parts.
     * @param partsSceneInfo An array of @see PartSceneInfo containing
     *        the desired rendering parameters.
     * @pre   Provided Part ID in PartSceneInfo is valid. 
     * @post  Render scene is updated with new rendering parameter.
     */
    void setSceneState(PartSceneInfo[] partsSceneInfo);
    
    /**
     * Gets the rendering information for all parts. 
     */
    PartSceneInfo[] getSceneState();
    
    /**
     * Sets context mode.
     * When context mode is enabled, selected parts will be drawn 
     * in solid, while unselected parts will be drawn translucent.
     * When not in context mode, selected parts will be drawn with
     * gold highlight. 
     */
    void setContextMode(boolean contextMode);
    
    /**
     * Gets context mode.
     */
    boolean getContextMode();
    
    /**
     * Gets part IDs for all parts in the model.
     */
    int[] getPartIds();
    
    /**
     * Clears all part selections.
     */
    void clearSelections();
    
    /**
     * Sets part selection.
     * @pre Provided Part ID valid.
     */
    void setSelection(int partId, boolean select);
    
    /**
     * Gets part selection.
     * @pre Provided Part ID valid.
     */
    boolean getSelection(int partId);
    
    /**
     * Sets transformations of parts.
     * @param partIds           An array of IDs for parts that we would like to set the tranforms.
     * @param transformData     Transformations are represented as 4x4 transformation matrices.
     *                          Each transformation matrix is flattened into a 16-elements float
     *                          array in row order. The data is concatenated for multiple parts.
     *                          That is, for N parts, transformData should be N*16 elements.
     * @pre                     Provided Part IDs valid.
     */
    void setTransforms(int[] partIds, float[] transformsData);
    
    /**
     * Gets transformations of parts.
     * @param  partIds An array of IDs for parts that we would like to get the transforms.
     * @return N*16 elements float array representing transformation matrices of requested parts.
     * @pre    Provided Part IDs valid.
     */
    float[] getTransforms(int[] partIds);
    
    /**
     * Given renderer viewport coordinates, return the ID of part located at the position.
     * Note that the input coordinates is relative to the renderer's internal viewport. The
     * GLSurface that the renderer outputs to may be of different size from the renderer's
     * viewport. Ensure to map from GLSurface coordinates to renderer's internal coordinates.
     * @return Part ID of selected part. Returns -1 if no part selected.
     */
    int PickPartFromScreen(float x, float y);
    static final int NoPartPicked = -1;
}


