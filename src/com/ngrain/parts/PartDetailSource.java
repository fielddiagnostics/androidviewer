package com.ngrain.parts;

import java.util.HashMap;
import java.util.Map;

import com.ngrain.parts.contracts.PartDetailsModel;

public class PartDetailSource implements PartDetailsModel
{
    public PartDetails GetDetails(int partId)
    {
        if (partId == NoPartSelection)
        {
            return null;
        }
        if (mData.containsKey(partId))
        {
            return mData.get(partId);
        }
        PartDetails details = BuildPartDetails(partId);
        mData.put(partId, details);
        return details;
    }

    private static PartDetails BuildPartDetails(Integer partId)
    {
        PartDetails details = new PartDetails();
        nativeGetPartLabels(partId, details);
        return details;
    }

    private final Map<Integer, PartDetails> mData =
        new HashMap<Integer, PartDetails>();

    private static native void nativeGetPartLabels(
        int partId, PartDetails details);

    private static final int NoPartSelection = -1;
}
