package com.ngrain.parts.ioc;

import org.picocontainer.MutablePicoContainer;

import android.content.Context;
import android.widget.TextView;

import com.ngrain.android.vrdemo.R;
import com.ngrain.android.vrdemo.ViewProvider;
import com.ngrain.parts.PartDetailSource;
import com.ngrain.parts.PartDetailsController;
import com.ngrain.parts.PartDetailsDisplay;
import com.ngrain.parts.PartSelection;
import com.ngrain.parts.contracts.PartDetailsView;

/**
 * Registration of concrete classes in Parts module with the IoC container.
 */
public class PartsContainerModule
{
    public static void Register(MutablePicoContainer container)
    {
        container.addComponent(PartSelection.class);
        container.addComponent(PartDetailSource.class);
        container.addComponent(PartDetailsController.class);
    }

    public static void RegisterViews(
        MutablePicoContainer container,
        ViewProvider viewProvider,
        Context context)
    {
        TextView detailKeyView =
            (TextView) viewProvider.getOverlayElementById(
                R.id.partDetailKeyTextView);
        TextView detailValueView = 
            (TextView) viewProvider.getOverlayElementById(
                R.id.partDetailValueTextView);
        TextView partNameView =
            (TextView) viewProvider.getOverlayElementById(
                R.id.partNameTextView);

        PartDetailsView view = new PartDetailsDisplay(
            detailKeyView, detailValueView, partNameView);

        container.addComponent(view);
    }
}
