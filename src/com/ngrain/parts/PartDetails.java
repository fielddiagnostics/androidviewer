package com.ngrain.parts;

import java.util.HashMap;
import java.util.Map;

public class PartDetails
{
    public Map<String, String> getValues()
    {
        return mValues;
    }

    public PartDetails()
    {
        mValues = new HashMap<String, String>();
    }

    private Map<String, String> mValues;
}
