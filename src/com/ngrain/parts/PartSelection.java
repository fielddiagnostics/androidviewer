package com.ngrain.parts;

import com.ngrain.parts.contracts.PartPickedListener;
import com.ngrain.parts.contracts.PartPicker;
import com.ngrain.scene.contracts.SceneController;

public class PartSelection implements PartPicker
{
    public PartSelection(SceneController sceneController)
    {
        mSceneController = sceneController;
        mLastSelectedParts = null;
    }

    public void registerListener(PartPickedListener listener)
    {
        mListener = listener;
    }

    public void restorePreviousSelection()
    {
        mSceneController.clearSelections();
        
        for( int partId : mLastSelectedParts )
        {
            mSceneController.setSelection(partId, true);
        }
    }

    public void deselectParts()
    {
        mSceneController.clearSelections();
    }

    public void pickPart(int partId)
    {
        deselectParts();
        
        if( partId == NoPartSelection )
        {
            return;
        }
        
        mSceneController.setSelection(partId, true);
        
        mLastSelectedParts = new int[] {partId};
        onPartSelected();
        
    }

    public void pickParts(int[] partIds)
    {
        deselectParts();
        
        if (partIds == null || partIds.length == 0)
        {
            return;
        }

        for( int partId : partIds )
        {
            mSceneController.setSelection(partId, true);
        }
        
        mLastSelectedParts = partIds;
        onPartSelected();
    }

    private void onPartSelected()
    {
        if (mListener != null)
        {
            mListener.onPartPicked(mLastSelectedParts);
        }
    }

    private int mLastSelectedParts[];
    private PartPickedListener mListener;
    private SceneController mSceneController;
    private static final int NoPartSelection = -1;
}
