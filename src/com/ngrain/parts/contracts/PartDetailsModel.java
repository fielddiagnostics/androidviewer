package com.ngrain.parts.contracts;

import com.ngrain.parts.PartDetails;

/**
 * The "Model" of the parts detail feature. Other than 3D geometry,
 * each part can have associated name, description, and other attributes.
 * The parts detail feature loads and displays the details.
 */
public interface PartDetailsModel
{
    /**
     * Get part details given part ID.
     * @param partId Part ID.
     * @pre   partID is valid
     */
    public PartDetails GetDetails(int partId);
}
