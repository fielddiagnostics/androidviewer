package com.ngrain.parts.contracts;

/**
 * Callback interface to get notified by PartPicker of changes in 
 * part selection.
 */
public interface PartPickedListener
{
    /**
     * Called by PartPicker when part selection changed.
     * @param partIds Array of IDs of selected parts.
     */
    void onPartPicked(int[] partIds);
}
