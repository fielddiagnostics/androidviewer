package com.ngrain.parts.contracts;

/**
 * The "View" of the parts detail feature. Other than 3D geometry,
 * each part can have associated name, description, and other attributes.
 * The parts detail feature loads and displays the details.
 */
public interface PartDetailsView
{
    /**
     * Displays the given part name.
     * @param name Part name.
     */
    void setPartName(String name);
    
    /**
     * Displays part detail title or key. E.g. Stock level
     * @param key Part detail title.
     */
    void setDetailKey(String key);
    
    /**
     * Displays part detail value associated with the title 
     * E.g. '5' for Stock level
     * @param value Part detail value.
     */
    void setDetailValue(String value);
    
    /**
     * Toggle part details visibility
     * @param visible If true shows part detail.
     */
    void setDetailsVisible(boolean visible);
}
