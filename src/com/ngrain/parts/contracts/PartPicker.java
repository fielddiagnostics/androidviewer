package com.ngrain.parts.contracts;

/**
 * Controller for showing part selection in 3D scene.
 */
public interface PartPicker
{
    /**
     * Registers a listener to notify when part selection is changed.
     * Only 1 listener can be registered at a time.
     */
    void registerListener(PartPickedListener listener);
    
    /**
     * De-select all parts.
     */
    void deselectParts();
    
    /**
     * Highlight a part as selection, while de-selecting all other parts.
     * @param   partID ID of part to be highlighted. In the case ID is -1
     *          then no part is highlighted.
     * @pre     partId is valid, or -1. 
     */
    void pickPart(int partId);
    
    /**
     * Highlight given parts, while de-selecting all other parts.
     * @param   partIDs Array of IDs of parts to be highlighted. 
     * @pre     Elements of partIds valid. 
     */
    void pickParts(int[] partIds);
    
    /**
     * Restore previous part selections.
     */
    void restorePreviousSelection();
}
