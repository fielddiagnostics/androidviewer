package com.ngrain.parts;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import com.ngrain.parts.contracts.PartDetailsModel;
import com.ngrain.parts.contracts.PartDetailsView;

import android.os.Handler;

public class PartDetailsController
{
    public PartDetailsController(PartDetailsModel source, PartDetailsView view)
    {
        mSource = source;
        mView = view;
        mView.setDetailsVisible(false);

        mHandler = new Handler();
        mRunnable = new DetailsRunnable();
    }

    public void reset()
    {
        updateSelection(-1);
    }

    public void updateSelection(int partId)
    {
        mSelection = mSource.GetDetails(partId);

        if (mSelection == null)
        {
            mHandler.removeCallbacks(mRunnable);
            mDetailsIter = null;

            mView.setDetailsVisible(false);
            
            mView.setDetailKey(NoDetails);
            mView.setDetailValue(NoDetails);
            mView.setPartName(NoDetails);
            
            return;
        }
        
        mView.setDetailsVisible(true);
        
        String name = nativeGetPartNameFromId(partId);
        mView.setPartName(name);
        
        startTimer();
    }

    private void startTimer()
    {
        mHandler.removeCallbacks(mRunnable);
        mHandler.postDelayed(mRunnable, DisplayDelayMs);
    }

    private class DetailsRunnable implements Runnable
    {
        public void run()
        {
            //If no part was selected
            if (mSelection == null)
            {
                return;
            }

            //If this is the first time we get parts detail or that
            //we have looped thru all part detail attributes
            if (mDetailsIter == null || !mDetailsIter.hasNext())
            {
                mDetailsIter = mSelection.getValues().entrySet().iterator();
            }
            
            //If the part has no associated detail attributes
            if (!mDetailsIter.hasNext())
            {
                mView.setDetailKey(NoDetails);
                mView.setDetailValue(NoDetails);
                return;
            }
            
            Entry<String, String> detail = mDetailsIter.next();

            mView.setDetailKey(detail.getKey());
            mView.setDetailValue(detail.getValue());

            mHandler.postDelayed(this, DisplayTimeMs);
        }
    }

    private PartDetails mSelection;
    private Iterator<Map.Entry<String, String>> mDetailsIter;

    private final PartDetailsModel mSource;
    private final PartDetailsView mView;

    private final Handler mHandler;
    private final Runnable mRunnable;

    private static final int DisplayDelayMs = 0;
    private static final int DisplayTimeMs = 3300;
    private static final String NoDetails = "";

    private static native String nativeGetPartNameFromId(int partId);
}
