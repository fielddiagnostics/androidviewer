package com.ngrain.parts;

import android.view.View;
import android.widget.TextView;

import com.ngrain.parts.contracts.PartDetailsView;

public class PartDetailsDisplay implements PartDetailsView
{
    public PartDetailsDisplay(
        TextView keyView, 
        TextView valueView, 
        TextView partNameView)
    {
        mKeyView = keyView;
        mValueView = valueView;
        mPartNameView = partNameView;
    }

    public void setPartName(String name)
    {
        mPartNameView.setText(name);
    }

    public void setDetailKey(String key)
    {
        mKeyView.setText(key);
    }

    public void setDetailValue(String value)
    {
        mValueView.setText(value);
    }

    public void setDetailsVisible(boolean visible)
    {
        if (visible)
        {
            mPartNameView.setVisibility(View.VISIBLE);
            mKeyView.setVisibility(View.VISIBLE);
            mValueView.setVisibility(View.VISIBLE);
        }
        else
        {
            mPartNameView.setVisibility(View.GONE);
            mKeyView.setVisibility(View.GONE);
            mValueView.setVisibility(View.GONE);
        }
    }

    private final TextView mKeyView;
    private final TextView mValueView;
    private final TextView mPartNameView;
}
