package com.ngrain.camera.contracts;

/**
 * Represents the virtual camera position using spherical coordinates and 
 * the camera target look-at. The camera is first positioned using the 3 spherical
 * rotation parameters. After performing the spherical rotations the camera would
 * still be looking at the origin. The camera is then translated so that it is aimed at
 * the desired look-at.
 */
public class CameraProperties 
{
    /**
     * Constructor
     
     * @param latitude      Latitude of the camera [deg]
     * @param longitude     Longitude of the camera [deg]
     * @param rotation      Rotation of the camera about its optical axis [deg]
     * @param targetX       Look-at X coordinates
     * @param targetY       Look-at Y coordinates
     * @param targetZ       Look-at Z coordinates            
     */
    public CameraProperties(
        double latitude,
        double longitude,
        double rotation,
        double zoom,
        double targetX,
        double targetY,
        double targetZ )
    {
        mLatitude = latitude;
        mLongitude = longitude;
        mRotation = rotation;
        mZoom = zoom;
        mTargetX = targetX;
        mTargetY = targetY;
        mTargetZ = targetZ;
    }
    
    public double getLatitude()
    {
        return mLatitude;
    }
    
    public double getLongitude()
    {
        return mLongitude;
    }
    
    public double getRotation()
    {
        return mRotation;
    }
    
    public double getZoom()
    {
        return mZoom;
    }
    
    public double getTargetX()
    {
        return mTargetX;
    }
    
    public double getTargetY()
    {
        return mTargetY;
    }
    
    public double getTargetZ()
    {
        return mTargetZ;
    }
    
    private final double mLatitude;
    private final double mLongitude;
    private final double mRotation;
    private final double mZoom;
    private final double mTargetX;
    private final double mTargetY;
    private final double mTargetZ;
}
