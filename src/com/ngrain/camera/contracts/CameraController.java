package com.ngrain.camera.contracts;

/**
 * Allows one to change the virtual camera position in the 3D
 * scene. Provides several ways of specifying camera position.
 */
public interface CameraController 
{
    /**
     * Sets the camera position using @See CameraProperties.
     * @param properties The camera property that one would like to
     *        set to.
     */
    void setCameraProperties(CameraProperties properties);
    
    /**
     * Sets the camera position using transformation matrix.
     * @param transformData Represents camera's transformation matrix
     *        which is flattened into a 16-elements float array stored in
     *        row order.
     */
    void setCameraTransform(float[] transformData);
}
