package com.ngrain.tasks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ngrain.tasks.contracts.TasksModel;

public class TaskSource implements TasksModel
{
    public List<StepProperties> getSteps(String taskId)
    {
        if (taskId == null)
        {
            return null;
        }
        if (mData.containsKey(taskId))
        {
            return mData.get(taskId);
        }
        List<StepProperties> steps = buildTaskSteps(taskId);
        mData.put(taskId, steps);
        return steps;
    }

    private static List<StepProperties> buildTaskSteps(String taskId)
    {
        StepProperties[] steps = nativeGetTaskSteps(taskId);
        return new ArrayList<StepProperties>(Arrays.asList(steps));
    }

    private final Map<String, List<StepProperties>> mData =
        new HashMap<String, List<StepProperties>>();

    private static native StepProperties[] nativeGetTaskSteps(String taskId);
}
