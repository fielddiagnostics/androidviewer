package com.ngrain.tasks;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import android.util.SparseArray;

import com.ngrain.android.vrdemo.Menu;
import com.ngrain.android.vrdemo.R;
import com.ngrain.tasks.contracts.TaskDiscovery;

public class TaskSelectionController
{
    public TaskSelectionController(TaskDiscovery source, Menu menu)
    {
        insertMenuItems(source, menu);
    }

    public String getSelectedTaskId(int menuId)
    {
        // get() returns null if menuId is not found
        return mMenuIdTaskIdMap.get(menuId);
    }

    private void insertMenuItems(TaskDiscovery source, Menu menu)
    {
        mMenuIdTaskIdMap = new SparseArray<String>();
        DiscoveredTasks taskMap = source.getAvailableTasks();
        Iterator<Map.Entry<String, String>> iter =
            taskMap.getValues().entrySet().iterator();

        while (iter.hasNext())
        {
            Entry<String, String> task = iter.next();
            String taskId = task.getKey();
            String taskName = task.getValue();

            menu.insertMenuItem(taskId.hashCode(),
                taskName,
                R.drawable.icon_procedures,
                0);

            mMenuIdTaskIdMap.put(taskId.hashCode(), taskId);
        }
    }

    private SparseArray<String> mMenuIdTaskIdMap;
}
