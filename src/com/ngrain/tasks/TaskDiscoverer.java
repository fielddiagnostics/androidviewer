package com.ngrain.tasks;

import com.ngrain.tasks.contracts.TaskDiscovery;

public class TaskDiscoverer implements TaskDiscovery
{
    public DiscoveredTasks getAvailableTasks()
    {
        if (mData == null)
        {
            mData = buildDiscoveredTasks();
        }
        return mData;
    }

    private static DiscoveredTasks buildDiscoveredTasks()
    {
        DiscoveredTasks tasks = new DiscoveredTasks();
        nativeGetAvailableTasks(tasks);
        return tasks;
    }

    private DiscoveredTasks mData;
    private static native void nativeGetAvailableTasks(DiscoveredTasks details);
}
