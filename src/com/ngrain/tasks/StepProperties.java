package com.ngrain.tasks;

import com.ngrain.camera.contracts.CameraProperties;
import com.ngrain.scene.contracts.PartSceneInfo;

public class StepProperties
{
    public enum StepType
    {
        Selection(0),
        Unsupported(1);

        private StepType(int value)
        {
            this.value = value;
        }

        private final int value;
    }

    public StepType getType()
    {
        return mType;
    }

    public String getId()
    {
        return mId;
    }

    public String getName()
    {
        return mName;
    }

    public String getDescription()
    {
        return mDescription;
    }

    public String getWarning()
    {
        return mWarning;
    }

    public String getAnimationId()
    {
        return mAnimationId;
    }
    
    public boolean getContextMode()
    {
        return mContextMode;
    }

    public PartSceneInfo[] getPartsSceneInfo()
    {
        return mPartsSceneInfo;
    }
    
    public CameraProperties getCameraProperties()
    {
        return mCameraProperties;
    }

    public StepProperties(
        int type,
        String id,
        String name,
        String desc,
        String warning,
        String animationId,
        boolean contextMode,
        PartSceneInfo[] partsInfo,
        CameraProperties cameraProperties)
    {
        mType = StepType.values()[type];
        mId = stripWhiteSpace(id);
        mName = name;
        mDescription = desc;
        mWarning = warning;
        mAnimationId = stripWhiteSpace(animationId);
        mContextMode = contextMode;
        mPartsSceneInfo = partsInfo;
        mCameraProperties = cameraProperties;
    }

    private static String stripWhiteSpace(String input)
    {
        return input.replaceAll("\\s", "");
    }

    private final StepType mType;
    private final String mId;
    private final String mName;
    private final String mDescription;
    private final String mWarning;
    private final String mAnimationId;
    private final boolean mContextMode;
    private final PartSceneInfo[] mPartsSceneInfo;
    private final CameraProperties mCameraProperties;
}
