package com.ngrain.tasks;

import com.ngrain.animations.contracts.AnimationPlayer;
import com.ngrain.camera.contracts.CameraController;
import com.ngrain.parts.contracts.PartPicker;
import com.ngrain.scene.contracts.SceneController;
import com.ngrain.tasks.StepProperties.StepType;
import com.ngrain.tasks.contracts.TaskStepPlayer;
import com.ngrain.tasks.contracts.TaskStepView;

public class StepPlayback implements TaskStepPlayer
{
    public StepPlayback(
        AnimationPlayer player,
        TaskStepView stepView,
        CameraController cameraController,
        SceneController sceneController)
    {
        mPlayer = player;
        mStepView = stepView;
        mCameraController = cameraController;
        mSceneController = sceneController;
        mStepView.setDetailsVisible(false);
    }

    public void playStep(StepProperties step)
    {
        mStepView.setDetailsVisible(true);
        mStepView.setName(step.getName());
        mStepView.setDescription(step.getDescription());

        //Stop any previous animations
        mPlayer.stopAnimation();
        
        String animationId = step.getAnimationId();
        if (animationId != null && animationId != "")
        {
            mPlayer.playAnimation(animationId);
        }
        else
        {
            mSceneController.setContextMode(step.getContextMode());
            mSceneController.setSceneState(step.getPartsSceneInfo());
            mCameraController.setCameraProperties(step.getCameraProperties());
        }
        
        String warning = step.getWarning();
        if (warning != null && !warning.isEmpty())
        {
            mStepView.displayWarning(warning);
        }
        else
        {
            mStepView.dismissWarning();
        }
    }

    public void stop()
    {
        mPlayer.stopAnimation();
        mStepView.dismissWarning();
        mStepView.setDetailsVisible(false);
        mStepView.setName(NoDetails);
        mStepView.setDescription(NoDetails);
    }

    private final AnimationPlayer mPlayer;
    private final TaskStepView mStepView;
    private final CameraController mCameraController;
    private final SceneController mSceneController;
    private static final String NoDetails = "";
}
