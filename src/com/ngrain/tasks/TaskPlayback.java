package com.ngrain.tasks;

import java.util.Iterator;

import com.ngrain.parts.contracts.PartPicker;
import com.ngrain.tasks.contracts.TaskPlayer;
import com.ngrain.tasks.contracts.TaskStepPlayer;
import com.ngrain.tasks.contracts.TasksModel;

public class TaskPlayback implements TaskPlayer
{
    public TaskPlayback(
        TasksModel source, TaskStepPlayer stepPlayer, PartPicker partPicker)
    {
        mTaskSource = source;
        mStepPlayer = stepPlayer;
        mPartPicker = partPicker;
    }

    public void startTask(String taskId)
    {
        if( mIsPlayingTask )
        {
            stopTask();
        }
        
        mIsPlayingTask = true;
        mPartPicker.deselectParts();
        mTaskIter = mTaskSource.getSteps(taskId).iterator();
        nextTaskStep();
    }

    public boolean nextTaskStep()
    {
        if (!mTaskIter.hasNext())
        {
            stopTask();
            return false;
        }

        StepProperties step = mTaskIter.next();
        mStepPlayer.playStep(step);
        return true;
    }

    public void stopTask()
    {
        if( !mIsPlayingTask ) return;
        
        mStepPlayer.stop();
        mIsPlayingTask = false;
    }

    public boolean isPlayingTask()
    {
        return mIsPlayingTask;
    }

    private boolean mIsPlayingTask = false;
    private Iterator<StepProperties> mTaskIter;

    private final TasksModel mTaskSource;
    private final TaskStepPlayer mStepPlayer;
    private final PartPicker mPartPicker;
}
