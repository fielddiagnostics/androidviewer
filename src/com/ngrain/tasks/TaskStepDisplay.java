package com.ngrain.tasks;

import com.ngrain.tasks.contracts.TaskStepView;

import android.view.View;
import android.widget.TextView;

public class TaskStepDisplay implements TaskStepView
{
    public TaskStepDisplay(
        TextView taskStepNameView,
        TextView taskStepDescriptionView,
        TextView warningTextView)
    {
        mStepNameView = taskStepNameView;
        mStepDescriptionView = taskStepDescriptionView;
        mWarningTextView = warningTextView;
    }

    public void setName(String name)
    {
        mStepNameView.setText(name);
    }
    
    public void setDescription(String comment)
    {
        mStepDescriptionView.setText(comment);
    }
    
    public void setDetailsVisible(boolean visible)
    {
        if (visible)
        {
            mStepNameView.setVisibility(View.VISIBLE);
            mStepDescriptionView.setVisibility(View.VISIBLE);
        }
        else
        {
            mStepNameView.setVisibility(View.GONE);
            mStepDescriptionView.setVisibility(View.GONE);
        }
    }

    public void displayWarning(String message)
    {
        mWarningTextView.setText(message);
        mWarningTextView.setVisibility(View.VISIBLE);
    }

    public void dismissWarning()
    {
        mWarningTextView.setText("");
        mWarningTextView.setVisibility(View.GONE);
    }

    private final TextView mStepNameView;
    private final TextView mStepDescriptionView;
    private final TextView mWarningTextView;
    
    
}
