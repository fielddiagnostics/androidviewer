package com.ngrain.tasks.contracts;

import java.util.List;

import com.ngrain.tasks.StepProperties;

/**
 * The "Model" of the task playback feature. 
 */
public interface TasksModel
{
    /**
     * Provides task steps associated with a task.
     * @param taskId
     * @pre taskId is valid.
     * @return List of steps associate with the task. 
     */
    List<StepProperties> getSteps(String taskId);
}
