package com.ngrain.tasks.contracts;

import com.ngrain.tasks.StepProperties;

/**
 * The "Controller" of the task step playback feature. Coordinates
 * with views and other controllers to perform task step playback.
 */
public interface TaskStepPlayer
{
    /**
     * Plays the given task step.
     */
    void playStep(StepProperties step);
    
    /**
     * Stops task step playback.
     */
    void stop();
}
