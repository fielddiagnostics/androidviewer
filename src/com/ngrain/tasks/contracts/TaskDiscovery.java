package com.ngrain.tasks.contracts;

import com.ngrain.tasks.DiscoveredTasks;

/**
 * The "Model" of the task playback feature. Provides the available
 * tasks associated with the loaded file.
 */
public interface TaskDiscovery
{
    /**
     * @return Map of <TaskId, TaskName> pairs.
     */
    DiscoveredTasks getAvailableTasks();
}
