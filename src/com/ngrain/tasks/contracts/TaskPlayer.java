package com.ngrain.tasks.contracts;

/**
 * The "Controller" of the task playback feature. Coordinates
 * with views and other controllers to perform task playback.
 */
public interface TaskPlayer
{
    /**
     * Start task play back given task ID.
     * @param taskId ID of task to be played.
     * @pre taskId is valid.
     */
    void startTask(String taskId);
    
    /**
     * Starts the next task step play back.
     * @return False if there are no more steps, otherwise true.
     */
    boolean nextTaskStep();
    
    /**
     * Stops task play back.
     */
    void stopTask();
    
    /**
     * Checks if in the middle of task playback.
     * @return True if in middle of playback.
     */
    boolean isPlayingTask();
}
