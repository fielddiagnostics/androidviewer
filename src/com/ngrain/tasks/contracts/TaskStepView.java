package com.ngrain.tasks.contracts;

/**
 * The "View" of the task step playback feature. Displays
 * task step related attributes such as step name, description, ...
 */
public interface TaskStepView
{
    /**
     * Display the name of the task step.
     * @param name Task step name.
     */
    void setName(String name);
    
    /**
     * Display the description of the task step.
     * @param description Task step description.
     */
    void setDescription(String description);
    
    /**
     * Toggles visibility of task step details display.
     * @param visible True to show details, otherwise false.
     */
    void setDetailsVisible(boolean visible);
    
    /**
     * Pop-up a warning dialog.
     * @param message Contents of warning dialog.
     */
    void displayWarning(String message);
    
    /**
     * Dismiss a warning dialog.
     */
    void dismissWarning();
}
