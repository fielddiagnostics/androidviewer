package com.ngrain.tasks;

import java.util.HashMap;
import java.util.Map;

//<Key, Value> maps to <TaskId, TaskName>
public class DiscoveredTasks
{
    public Map<String, String> getValues()
    {
        return mValues;
    }

    public DiscoveredTasks()
    {
        mValues = new HashMap<String, String>();
    }

    private Map<String, String> mValues;
}
