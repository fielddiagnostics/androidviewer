package com.ngrain.tasks.ioc;

import org.picocontainer.MutablePicoContainer;

import android.content.Context;
import android.widget.TextView;

import com.ngrain.android.vrdemo.R;
import com.ngrain.android.vrdemo.ViewProvider;

import com.ngrain.tasks.TaskDiscoverer;
import com.ngrain.tasks.TaskPlayback;
import com.ngrain.tasks.StepPlayback;
import com.ngrain.tasks.TaskSelectionController;
import com.ngrain.tasks.TaskStepDisplay;
import com.ngrain.tasks.TaskSource;
import com.ngrain.tasks.contracts.TaskStepView;

/**
 * Registration of concrete classes in Task module with the IoC container.
 */
public class TasksContainerModule
{
    // Discovery components usually need to be registered
    // earlier on in the life cycle, hence a separate registry.
    public static void RegisterDiscovery(MutablePicoContainer container)
    {
        container.addComponent(TaskDiscoverer.class);
        
    }

    public static void Register(MutablePicoContainer container)
    {
        container.addComponent(TaskSource.class);
        container.addComponent(StepPlayback.class);
        container.addComponent(TaskPlayback.class);
    }

    public static void RegisterViews(
        MutablePicoContainer container,
        ViewProvider viewProvider,
        Context context)
    {
        TextView taskStepNameView = (TextView) viewProvider.getOverlayElementById(
            R.id.taskStepNameTextView);
        
        TextView taskStepDescriptionView = (TextView) viewProvider.getOverlayElementById(
            R.id.taskStepDescriptionView);

        TextView warningTextView = (TextView) viewProvider.getOverlayElementById(
            R.id.warningTextView);

        TaskStepView stepView = new TaskStepDisplay(
            taskStepNameView,
            taskStepDescriptionView,
            warningTextView);
               
        container.addComponent(stepView);
    }
}
