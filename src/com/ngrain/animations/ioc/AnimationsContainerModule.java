package com.ngrain.animations.ioc;

import org.picocontainer.MutablePicoContainer;

import com.ngrain.animations.AnimationPlayback;
import com.ngrain.animations.AnimationSource;

/**
 * Registration of concrete classes in Animation module with the IoC container.
 */
public class AnimationsContainerModule
{
    public static void Register(MutablePicoContainer container)
    {
        container.addComponent(AnimationSource.class);
        container.addComponent(AnimationPlayback.class);
    }
}
