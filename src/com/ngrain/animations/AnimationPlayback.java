package com.ngrain.animations;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ngrain.animations.contracts.AnimationModel;
import com.ngrain.animations.contracts.AnimationPlayer;
import com.ngrain.animations.contracts.KeyFrame;
import com.ngrain.camera.contracts.CameraController;
import com.ngrain.camera.contracts.CameraProperties;
import com.ngrain.scene.contracts.PartSceneInfo;
import com.ngrain.scene.contracts.SceneController;

import android.os.Handler;
import android.util.Log;

public class AnimationPlayback implements AnimationPlayer
{
    public AnimationPlayback(
        AnimationModel animationModel,
        SceneController sceneController,
        CameraController cameraController)
    {
        mAnimationModel = animationModel;
        mSceneController = sceneController;
        mCameraController = cameraController;
        mHandler = new Handler();
        mRunnable = new PlaybackRunnable();
    }

    public void playAnimation(String animationId)
    {
        if( mAnimationPlaying )
        {
            stopAnimation();
        }
        
        mKeyframes = mAnimationModel.getKeyFrames(animationId);
        
        mAnimationPlaying = true;
        
        mCurrentAnimationId = animationId;
        mCurrentAnimationLength = mKeyframes.get(mKeyframes.size()-1).getTime();

        mLoopAnimation = true;
        
        // Set the "start state" of animation before playing.
        mElapsedMs = 0;
        mCurrLowerBoundKeyframe = 0;
        mCurrUpperBoundKeyframe = 1;
        mSceneController.setSceneState(mKeyframes.get(0).getPartsSceneInfo());
        mSceneController.setContextMode(mKeyframes.get(0).getContextMode());
        mCameraController.setCameraProperties(mKeyframes.get(0).getCameraProperties());
        setInterpolationBounds(mCurrLowerBoundKeyframe, mCurrUpperBoundKeyframe);

        //Start the timer playback mechanism
        mHandler.postDelayed(mRunnable, PlayDelayMs);
    }
    
    public void stopAnimation()
    {
        if( !mAnimationPlaying ) return;
        
        mHandler.removeCallbacks(mRunnable);
        
        mAnimationPlaying = false;
    }

    private class PlaybackRunnable implements Runnable
    {
        public void run()
        {
            mElapsedMs += PlaybackTickMs;
            double elapsedSeconds = (double) mElapsedMs / 1000.0;
            
            //Find upper bound keyframe given current play time position
            int upperBoundKeyframe = getUpperBoundKeyframeNonInclusive(elapsedSeconds);
            
            //Check if upperBoundKeyframe makes sense
            assert( upperBoundKeyframe > 0 );
            assert( upperBoundKeyframe >= mCurrUpperBoundKeyframe);
            assert( upperBoundKeyframe <= mKeyframes.size());
            
            //If upper bound keyframe has changed
            if( upperBoundKeyframe != mCurrUpperBoundKeyframe )
            {
                //Update the bound keyframes
                mCurrUpperBoundKeyframe = upperBoundKeyframe;
                mCurrLowerBoundKeyframe = upperBoundKeyframe - 1;
                
                //Set scene state using the lower bound inclusive keyframe
                mSceneController.setSceneState(
                    mKeyframes.get(mCurrLowerBoundKeyframe).getPartsSceneInfo());
                
                mSceneController.setContextMode(mKeyframes.get(mCurrLowerBoundKeyframe).getContextMode());
                
                //Set the camera pose using lower bound inclusive keyframe
                mCameraController.setCameraProperties(mKeyframes.get(mCurrLowerBoundKeyframe).getCameraProperties());
                
                //Return if we have reached the end of the animation. That is,
                //upperbound keyframe index is equal to number of keyframes.
                if (upperBoundKeyframe == mKeyframes.size())
                {
                    //Set animation stopped
                    mAnimationPlaying = false;
                    
                    //If looping is enabled call playAnimation again.
                    if( mLoopAnimation )
                    {
                        playAnimation(mCurrentAnimationId);
                    }
                    
                    return;
                }
                
                //Otherwise set interpolation bounds
                setInterpolationBounds(mCurrLowerBoundKeyframe, mCurrUpperBoundKeyframe);
            }
            
            //If we have not yet reached the end of the animation
            //compute the keypoint that which is a ratio between 0.0 to 1.0 indicating
            //how where we are between the 2 bounding keyframes
            double keypoint = (elapsedSeconds - mKeyframes.get(mCurrLowerBoundKeyframe).getTime())/mBoundFramesTimeIntervalSeconds;
            
//            Log.i(
//                "AnimationPlayer", 
//                "Frames [" + mCurrLowerBoundKeyframe + " " + mCurrUpperBoundKeyframe + "] time: " + elapsedSeconds + " keypoint: " + keypoint);
            
            //Interpolate and set moving part poses
            float[] interpolatedTransforms = nativeGetTransformsInterpolation(keypoint);
            
//            Log.i(
//                "AnimationPlayer", 
//                "Interpolated transforms: " + Arrays.toString(interpolatedTransforms));
            
            mSceneController.setTransforms(mMovingParts, interpolatedTransforms);
            
            //Interpolate and set camera pose
            float[] cameraTransform = nativeGetCameraInterpolation(keypoint);
            mCameraController.setCameraTransform(cameraTransform);
            
            //Continue posting event
            mHandler.postDelayed(this, PlaybackTickMs);
        }
    }
    
    private int getUpperBoundKeyframeNonInclusive(double time)
    {
        //Check if input time is within animation playback duration
        if( time >= mKeyframes.get(mKeyframes.size()-1).getTime() ) return mKeyframes.size();
        assert (time >= 0 );
        
        //Look for an upper bound index starting with current upper bound
        //keyframe index as starting point.
        int upperBoundIndex = mCurrUpperBoundKeyframe;
        while( time >= mKeyframes.get(upperBoundIndex).getTime() )
        {
            upperBoundIndex += 1;
            if( upperBoundIndex >= mKeyframes.size() )
            {
                upperBoundIndex = mKeyframes.size();
                break;
            }
        }
        
        //Search downwards to ensure we have an upper bound keyframe 
        //that is closest to input time.
        //Lower bound index is always upperBoundIndex - 1.
        //Lower bound is inclusive, that is if time is 0, then
        //lower bound keyframe index should be 0.
        int lowerBoundIndex = upperBoundIndex - 1;
        while( time < mKeyframes.get(lowerBoundIndex).getTime() )
        {
            lowerBoundIndex -= 1;
            assert( lowerBoundIndex >= 0 );
        }
        
        //Update upper bound index
        upperBoundIndex = lowerBoundIndex + 1;
        
        return upperBoundIndex;
    }
    
    private void setInterpolationBounds(int lowerKeyframeIndex, int upperKeyframeIndex)
    {
        KeyFrame upperFrame = mKeyframes.get(upperKeyframeIndex);
        KeyFrame lowerFrame = mKeyframes.get(lowerKeyframeIndex);
        PartSceneInfo[] endSceneState = upperFrame.getPartsSceneInfo();
        PartSceneInfo[] beginSceneState = lowerFrame.getPartsSceneInfo();
        
        //Track time interval between bounds
        mBoundFramesTimeIntervalSeconds = upperFrame.getTime() - lowerFrame.getTime();
        
        //Map PartId to PartSceneInfo
        mPartId2BeginState.clear();
        for(PartSceneInfo partInfo : beginSceneState)
        {
            mPartId2BeginState.put(partInfo.getPartId(), partInfo);
        }
        mPartId2EndState.clear();
        for(PartSceneInfo partInfo : endSceneState)
        {
            mPartId2EndState.put(partInfo.getPartId(), partInfo);
        }
        
        //Find all the moving parts and extract their transforms
        mMovingParts = upperFrame.getMovedParts();
        float [] beginTransformsData = new float[16*mMovingParts.length];
        float [] endTransformsData = new float[16*mMovingParts.length];
        for( int i = 0; i < mMovingParts.length; i++)
        {
            PartSceneInfo partBeginInfo = mPartId2BeginState.get(mMovingParts[i]);
            float[] partBeginTransform = partBeginInfo.getTransformData();
            PartSceneInfo partEndInfo = mPartId2EndState.get(mMovingParts[i]);
            float[] partEndTransform = partEndInfo.getTransformData();
            
            System.arraycopy(partBeginTransform, 0, beginTransformsData, i*16, 16);
            System.arraycopy(partEndTransform, 0, endTransformsData, i*16, 16);
        }
        
//        Log.i(
//            "AnimationPlayer", 
//            "Moving parts: " + 
//            Arrays.toString(mMovingParts) + 
//            " begin transform: " + 
//            Arrays.toString(beginTransformsData) + 
//            " end transform: " + 
//            Arrays.toString(endTransformsData));
            
        //Send begin/end transform data to C++ in preparation for interpolation
        nativeSetTransformsInterpolationBounds(beginTransformsData, endTransformsData);
        
        //Send camera begin/end properties to C++ in preparation for interpolation
        nativeSetCameraInterpolationBounds(lowerFrame.getCameraProperties(), upperFrame.getCameraProperties());
    }

    private final AnimationModel mAnimationModel;
    private final SceneController mSceneController;
    private final CameraController mCameraController;
    
    private boolean mLoopAnimation = false;
    private double mCurrentAnimationLength;
    private String mCurrentAnimationId;
    private boolean mAnimationPlaying = false;
 
    private long mElapsedMs = 0;
    private final Handler mHandler;
    private final PlaybackRunnable mRunnable;

    List<KeyFrame> mKeyframes;
    int mCurrLowerBoundKeyframe, mCurrUpperBoundKeyframe;
    
    Map<Integer, PartSceneInfo> mPartId2BeginState = new HashMap<Integer, PartSceneInfo>();
    Map<Integer, PartSceneInfo> mPartId2EndState = new HashMap<Integer, PartSceneInfo>();
    double mBoundFramesTimeIntervalSeconds;
    int [] mMovingParts;
    
    private static native void nativeSetTransformsInterpolationBounds(float[] beginTransformsData, float[] endTransformsData);
    private static native float[] nativeGetTransformsInterpolation(double keypoint);
    private static native void nativeSetCameraInterpolationBounds(CameraProperties beginCamera, CameraProperties endCamera);
    private static native float[] nativeGetCameraInterpolation(double keypoint);
    
    private static final int PlayDelayMs = 200;
    private static final int PlaybackTickMs = (int) (1000 * 1 / 20.0);
    private static final double PlaybackStartTime = 0.0;
}
