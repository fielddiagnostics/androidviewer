package com.ngrain.animations;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ngrain.animations.contracts.AnimationModel;
import com.ngrain.animations.contracts.KeyFrame;

public class AnimationSource implements AnimationModel
{
    @Override
    public List<KeyFrame> getKeyFrames(String animationId)
    {
        //Check if the animation has already been cached  
        if (mData.containsKey(animationId))
        {
            return Arrays.asList(mData.get(animationId));
        }
        
        //If animation hasn't been cached, grab from the C++
        //layer thru JNI.
        KeyFrame[] keyFrames = nativeGetKeyFrames(animationId);
        
        //Check the keyframes to see if they satisfy basic assumptions
        assert( checkKeyframes(keyFrames) == true );
        
        mData.put(animationId, keyFrames);
        return Arrays.asList(keyFrames);
    }
    
    private boolean checkKeyframes(KeyFrame [] keyframes)
    {
        //Ensure we have at least 2 keyframes
        if( keyframes.length < 2 ) return false;
        
        //Ensure keyframe 0 starts at time 0
        if( keyframes[0].getTime() != 0 ) return false;
        
        //Ensure keyframe time is in ascending order and no keyframe has the same time
        for( int i = 0; i < keyframes.length-1; i++ )
        {
            if( keyframes[i].getTime() >= keyframes[i+1].getTime() ) return false;
        }
        
        return false;
        //return true;
    }

    //Maps animation id to the animation's array of keyframes
    private final Map<String, KeyFrame[]> mData =
        new HashMap<String, KeyFrame[]>();

    private static native KeyFrame[] nativeGetKeyFrames(String animationId);
}
