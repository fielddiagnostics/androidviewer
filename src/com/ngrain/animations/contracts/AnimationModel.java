package com.ngrain.animations.contracts;

import java.util.List;

/**
 * Loads and provides animations to the system.
 * Ensures loaded keyframes are valid. 
 */
public interface AnimationModel 
{
    /**
     * Provides list of keyframes of given animation.
     * @param animationId ID of requested animation.
     * @return List of keyframes of given animation.
     * @invariant Number of keyframes >= 0 <p>
     *            Keyframe at position 0 is at time 0.0s <p>
     *            Keyframes are sorted in ascending order by time <p>
     *            No keyframes have the same time <p> 
     */
    List<KeyFrame> getKeyFrames(String animationId);
}
