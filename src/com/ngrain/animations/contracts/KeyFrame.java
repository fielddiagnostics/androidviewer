package com.ngrain.animations.contracts;

import com.ngrain.camera.contracts.CameraProperties;
import com.ngrain.scene.contracts.PartSceneInfo;

/**
 * Represents snapshot of the scene at a given time point in an animation.
 */
public class KeyFrame 
{
    /**
     * @return Time point of the animation [sec].
     */
    public double getTime()
    {
        return mTime;
    }
    
    /**
     * @return True if the scene at the time point is in context mode.
     */
    public boolean getContextMode()
    {
        return mContextMode;
    }

    /**
     * @return Rendering parameter for all the parts at the time point.
     */
    public PartSceneInfo[] getPartsSceneInfo()
    {
        return mPartsSceneInfo;
    }
    
    /**
     * @return List of parts that moved compared to previous keyframe.
     */
    public int[] getMovedParts()
    {
        return mMovedParts;
    }
    
    /**
     * @return Camera position at the time point.
     */
    public CameraProperties getCameraProperties()
    {
        return mCameraProperties;
    }

    /**
     * Constructor
     * @param time              Time point of the keyframe [sec]
     * @param contextMode       Whether to render in context mode at the time point
     * @param partsInfo         Rendering parameters for all parts in the scene
     * @param movedParts        Array of IDs of parts that moved compared to previous frame
     * @param cameraProperties  Virtual camera position in 3D scene at the time point.
     */
    public KeyFrame(
        double time,
        boolean contextMode,
        PartSceneInfo[] partsInfo,
        int[] movedParts,
        CameraProperties cameraProperties)
    {
        mTime = time;
        mContextMode = contextMode;
        mPartsSceneInfo = partsInfo;
        mMovedParts = movedParts;
        mCameraProperties = cameraProperties;
    }
    
    private final boolean mContextMode;
    private final double mTime;
    private final PartSceneInfo[] mPartsSceneInfo;
    private final int[] mMovedParts;
    private final CameraProperties mCameraProperties;
}
