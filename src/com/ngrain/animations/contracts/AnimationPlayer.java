package com.ngrain.animations.contracts;

/**
 * Controller for playback of animations.
 */
public interface AnimationPlayer
{
    /**
     * Plays animation given animation ID.
     */
    void playAnimation(String animationId);
    
    /**
     * Stops animation.
     */
    void stopAnimation();
}
