# README

This project is an Android-based viewer for training content created from NGRAIN Producer Pro. You can read more about NGRAIN and Producer Pro at [NGRAIN.com](http://www.ngrain.com). The viewer loads and displays 3D models; shows relevant parts information; and plays back maintenance tasks and animations.

The project code has two layers. The top layer is Java application code that includes UI, application logic, and any interaction with the Android operating system. The Java layer calls into a low-level C++ layer that is responsible for model loading and 3D rendering. Rendering is based on NGRAIN's proprietary C++ voxel renderer, same as that used in Producer. The renderer has been wrapped by high level Java interfaces, providing the application programmer flexibility to manipulate things such as the 3D scene or virtual camera. 

The project is maintained on Eclipse with Android Developer Tools plugin.

The project was contains unexpected and highly specific code to:

+ The ability to respond to Myo armband gestures by listening for gesture messages broadcast by third party Moverio app

+ The main activity of the app is also designed to be launched from another app and accepts a model file path to load.


To get started see project Wiki.